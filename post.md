This might turn out to be a longer post.

I have a monad transformer `SignalT sigs m a` where `sigs :: [*]` is a list of types that
can be signaled. Signals are like exceptions. They can be handled. Since this transformer
can throw any type in `sigs`, I want to provide a function `handleOne` that, given a handler for
a single type `s` in `sigs` it produces `SignalT sigs' m a` where ˙sigs'` is `sigs` without `s`.

Now, I've managed to do this with some typeclass magic so now I have this `HandleOne sigs s sigs'`
class that provides the `handleOne :: SignalT sigs m a -> (s -> b) -> (a -> b) -> SignalT sigs' m b`.

Now here's where the problem starts. I want to package this into a MTL style typeclass, `MonadHandleOne`.
First problem, what are it's parameters and what are their kinds? Ideally, this would be it

```haskell
class MonadHandleOne (m :: [*] -> (* -> *)) where
    handleOne :: (HandleOne sigs s sigs', Monad n)
              => m sigs n a -> (s -> b) -> (a -> b) -> m sigs' n b
```

(the `Monad n` part might be done better, not important).

This fits the instance

```haskell
instance MonadHandleOne SignalT where
    handleOne = SignalT.handleOne
```

Great. Except it fits no other instance. If the `SignalT` is inside of the transformer stack, say
`MaybeT (SignalT sigs) m a`, the `handleOne` function is still possible to implement, but there's
no way to fit this type in the instance.

I've tried two other things.

```haskell
class MonadHandleOne m sigs m' sigs' | ...sensible fundeps... where
    handleOne :: HandleOne sigs s sigs'
              => m a -> (s -> b) -> (a -> b) -> m' b
```

This fits all the instances I want but the problem is that the class is "too exposed".
I want to write actions with a signature like this `someAction :: (MonadHandleOne m [Int, String]) => m ()`

Any extra `m'` and `sigs'` don't appear in the head and are not determined from `m` and `sigs`.
I mean those parameters shouldn't even exist. They aren't some fixed types. This should be "handlable"
for any `s` in `sigs`. I couldn't get this to work.

The third attempt

```haskell
class MonadHandleOne m where
    type Signals m :: [*]
    type WithSignals m sigs :: * -> *
    handleOne :: HandleOne (Signals m) s sigs'
              => m a -> (s -> b) -> (a -> b) -> (WithSignals m sigs') b
```

This also fits the instances and I like this solution the best. However this code, for example, doesn't
typecheck.

```haskell
sample :: ('[Int, String] ~ Signals m, MonadHandleOne m) => m ()
sample = do
    signal (1 :: Int)
    signal "asd"

sample' :: ('[Int] ~ Signals m, MonadHandleOne m) => m ()
sample' = handleOne sample (\(_ :: String) -> ()) id
```

(the signal function is an extra function in the typeclass for actually constructing signals)

The error is this

```haskell
Could not deduce (m ~ WithSignals m0 sigs'0)
from the context ('[Int] ~ Signals m, MonadHandleOne m)
  bound by the type signature for
             sample' :: ('[Int] ~ Signals m, MonadHandleOne m) => m ()

  `m' is a rigid type variable bound by
      the type signature for
        sample' :: ('[Int] ~ Signals m, MonadHandleOne m) => m ()

Expected type: m ()
  Actual type: WithSignals m0 sigs'0 ()

Relevant bindings include
  sample' :: m ()

In the expression: handleOne (\ (_ :: String) -> ()) id sample
In an equation for sample':
    sample' = handleOne (\ (_ :: String) -> ()) id sample
```

```haskell
Could not deduce (Signals m0 ~ '[Int, String])
from the context ('[Int] ~ Signals m, MonadHandleOne m)
  bound by the type signature for
             sample' :: ('[Int] ~ Signals m, MonadHandleOne m) => m ()

The type variable `m0' is ambiguous
In the third argument of `handleOne', namely `sample'
In the expression: handleOne (\ (_ :: String) -> ()) id sample
In an equation for sample':
    sample' = handleOne (\ (_ :: String) -> ()) id sample
```

Basically, the root of these problems is that I somehow need to talk about an `m` that represents
any transformer stack BUT without the `sigs` applied to a `SignalT` inside. A sort of a type level
zipper. The added problem is that it has to work with unmodified transformers. I can't wrap them in
newtypes or anything.

I've been thinking about this for around a week and a half and I've exhausted what I know. Any ideas?
